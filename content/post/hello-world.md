+++
date = "2016-10-01T20:45:47+02:00"
draft = false
title = "Hello World"
author = "Matthias Metzger"
+++

Hi, my name is Matthias. For quite some time now, I have been wanting
to blog, primarily about my journey and learnings in software
development. Despite its seeming simplicity, setting up a blog with
all the possible choices concerning technologies and hosting can
become quite the challenge.

So in this post, I would like to show the tools and technologies I chose
to write this stuff. While striving for simplicity in my setup,
be aware, that most of this is probably rather for the technically
inclined. That shouldn't stop you from playing around with it,
however. If you just want to start blogging though - [as you should][1] -
I can recommend [Ghost][2] or [Wordpress][3].


### What is a blog, actually?

While writing this post, I asked myself, what word 'blog' was derived
from. Some words have quite the fascinating history - think about
'Eureka', for example. The oxford dictionary defines a blog as

> A regularly updated website or web page, [bla bla bla]
> that is written in an informal or conversational style. [1][4]

Well, that was rather boring. Alright, I confess: I justed wanted
something to quote to be able to play around with `<blockquote>`.

Now then, it seems 'blog' is the [shortened form][5] of *weblog*, a
composition of *World Wide Web* and *log*. This is interesting,
because I never realised a blog contains the word *log*. It also
means, I could sheepishly consider myself a captain of this blog,
since I am basically writing a log about my ship - Ahoy!

Errr... well, good nobody except me will read this stuff
anyways. Back to topic.


### Static site generators

While Wordpress and Ghost are perfectly fine for blogging purposes, I
personally don't like the added complexity of handling a database with
user credentials and dynamic content for essentially simple, static
text.

I also absolutely detest typing prose in editors which don't have good
Vim keybindings. Life is too short for that. Especially accidentally
typing `C-w` in a browser tab is the worst (deleting a word in Vim,
closing the tab in a browser). Of course, that only happens after
writing a thousand word rant..

Anyways, static site generators: Basically, you write your text in a
simple plaintext format, locally or wherever you want and they handle
converting that to HTML or any other publishing format. The most
common format probably is [Markdown][6] (don't quote me on that), but
others like [AsciiDoc][8] are available as well. Actually, as far as I
know, whole books have been written in AsciiDoc. The compiled HTML
can be put on any webserver under the sun and can be submitted to a
version control system as a nice side effect.

There is a [myriad of static site generators][9]. Since I like the
simplicity, out of the box speed and live reload capabilites offered
by [Hugo][10], I went with it. Creating a new project and starting up
a dev server is as simple as:

```bash
~ $ hugo new site blog
~ $ cd blog
~ $ hugo new post/hello-world.md
~/blog $ hugo server --buildDrafts
```

Awesome! Now I can write my stuff in my editor, watch the changes
livereload in a browser of my choosing and commit the work after being
done with it to version control.


### Editor

For all the editor junkies out there, after using Vim for about 5
years, I switched to [Emacs][11]. Bummer, I know. If you are a Vimmer
and have never tried Emacs, just give it a whirl, you might be
suprised. Everything Vim 8 is able to do, Emacs can do aswell. Oh and
don't forget to `M-x package install RET evil-mode RET`, because
otherwise RSI (repetitive strain injury) will haunt you.

If you are a programmer and use something else, like Atom, VS Studio
Code, Light Table or Brackets, I can just really recommend trying out
either Emacs or Vim. The learning curve is quite steep for both of
them, but just knowing them a little goes a long way. 

For all the others, that have come this far, Emacs (and Vim as well)
is a 40 years old text editor. Think about that for a second. They are
certainly older than me. For a little motiviation, on why Emacs might
be helpful, just take a look at [org-mode][7].

![Emacs and Hugo workflow][12]


### Hosting on GitHub or GitLab

With static site generators and the tools for writing out of the way,
thinking about where to host is the next step. Since I am using
[Git][13] for version control and need to keep the files somewhere, in case my
computer blows up, there are at least three options to consider:
[Bitbucket][14], [GitHub][15] or [GitLab][16].

All three of them enable open collaboration on projects via Git and
offer to host static sites (like landing pages or documentation) for
projects. Besides just being awesomely open and engaging, GitLab takes
it up a notch though. [Here][17] are several example projects for
static site generators (where we can find one for Hugo as well) and
their integration with GitLab Pages.

Since GitHub uses branching for projects, to be precise a gh-pages
branch, for determining which files to convert to HTML, I would either
need to build my posts locally and then push them to the specified
branch or have them somehow be built by TravisCI or CircleCI, with a
corresponding push to the gh-pages branch. I know it's not the only way,
for just a website, but for my intends and purposes, GitLab just won.

With GitLab, I can just create a project, named
`noobymatze.github.io`, copy the `.gitlab-ci.yaml` into my directory,
write a new post with Hugo, push it to the repo and wait for the
pipeline to run. Done. Take a look at the resulting built.

![GitLab pipeline passed successfully][18]

Of course, self hosting and setting it up would be a viable option as
well. I will probably do that at a later point, but for now, I just want
to start writing stuff and not bother with setting up a server securely.


### Styling and syntax highlighting

I tried to go without any extra tooling for styling, but I have to
concede, it's quite annoying. So, as always, I fell back on
using [Sass][19], although I hear [PostCSS][20] is all in the rage
nowadays. Also: [normalize.css][21] for making browsers behave. 

For syntax highlighting I just went with [highlight.js][22], because
it's a friggin mature dinosaur in web terms and has [Elm][23]
highlighting, which I haven't found for [prism.js][24] as of now. But
there is probably a plugin somewhere. There is always a plugin for JS
somewhere.


### Conclusion

That's it for now. If you got this far: Congrats, here is a [cat gif][25]
for your effort. If you just scrolled through and still watched the
gif: *Shame on you!*

```java
// Java
class HelloWorld {

  public static void main(String[] args) {
    System.out.println("Ahoy!");
  }

}
```

```haskell
-- Haskell
main :: IO ()
main = putStrLn "Hello, World!"
```

```elm
-- Elm
main : Html Never
main = 
  Html.text "Ahoy!"
```

```javascript
// JavaScript
console.log('Ahoy!');
```


[1]: https://sites.google.com/site/steveyegge2/you-should-write-blogs
[2]: https://ghost.org/
[3]: https://wordpress.com/
[4]: https://en.oxforddictionaries.com/definition/blog
[5]: http://drweb.typepad.com/dwdomain/2005/05/what_is_the_ori.html
[6]: https://daringfireball.net/projects/markdown/
[7]: https://www.youtube.com/watch?v=FtieBc3KptU
[8]: http://www.methods.co.nz/asciidoc/
[9]: https://www.staticgen.com/
[10]: https://gohugo.io/
[11]: https://www.gnu.org/software/emacs/
[12]: images/emacs-hugo-workflow.png
[13]: https://about.gitlab.com/
[14]: https://bitbucket.org/
[15]: https://github.com/
[16]: https://about.gitlab.com/
[17]: https://gitlab.com/groups/pages
[18]: images/gitlab-pipeline.png
[19]: http://sass-lang.com/
[20]: https://github.com/postcss/postcss
[21]: https://necolas.github.io/normalize.css/
[22]: https://highlightjs.org/
[23]: http://elm-lang.org/
[24]: http://prismjs.com/
[25]: http://img.pandawhale.com/post-10513-Code-Refactoring-Cat-in-Bathtu-U295.gif
